"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// require('./config/config');
var express = require("express");
var path = require("path");
var Server = /** @class */ (function () {
    function Server(port) {
        this.port = port;
        this.app = express();
    }
    Server.init = function (port) {
        return new Server(port);
    };
    Server.prototype.publicFolder = function () {
        var publicPath = path.resolve(__dirname, '../public');
        this.app.use(express.static(publicPath));
    };
    Server.prototype.start = function (callback) {
        this.app.listen(this.port, callback);
        this.publicFolder();
    };
    return Server;
}());
exports.default = Server;
// const bodyParser = require('body-parser');
// const app = express();
// // Parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }));
// //Parse application/json
// app.use(bodyParser.json());
// app.get('/usuario', function (req, res) {
//   res.json('getUsuario');
// });
// app.post('/usuario', function (req, res) {
//   res.json('postUsuario');
// });
// app.put('/usuario/:id', function (req, res) {
//   let body = req.body;
//   if ( body.nombre === undefined) {
//     res.status(400).json({
//       ok: false,
//       message: "El nombre es necesario"
//     });
//   }else{
//     res.json({ persona : body });
//   }
//   let id = req.params.id;
// });
// app.delete('/usuario', function (req, res) {
//   res.json('delete Usuario');
// });
// app.listen(process.env.PORT, () =>{
//   console.log('Escuchando Puerto 3000');
// });

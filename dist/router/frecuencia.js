"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var mysql_1 = __importDefault(require("../mysql/mysql"));
var evento = express_1.Router();
var rutaPython = '../../dist/hola.py';
evento.get('/frecuencia/guardarConfig', function (req, res) {
    console.log("Entra a la API");
    var amplitud = req.query.amplitud;
    var frecuencia = req.query.frecuencia;
    console.log('Entra petición');
    var query = "INSERT INTO log VALUES(null, " + frecuencia + ", " + amplitud + ", null)";
    mysql_1.default.ejecutarQuery(query, function (err, response) {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        }
        else {
            res.json({
                ok: true,
                response: response
            });
        }
    });
    console.log('Sale petición');
});
evento.get('/frecuencia/ejecutarScript', function (req, res) {
    var spawn = require('child_process').spawn;
    var pyprog = spawn('python', [rutaPython]);
    pyprog.stdout.on('data', function (data) {
        return 1;
    });
    pyprog.stderr.on('data', function (data) {
        return 0;
    });
});
exports.default = evento;

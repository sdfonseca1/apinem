"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var mysql_1 = __importDefault(require("../mysql/mysql"));
var evento = express_1.Router();
evento.get('/eventos', function (req, res) {
    var query = "SELECT *\n                 FROM eventos";
    mysql_1.default.ejecutarQuery(query, function (err, usuarios) {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        }
        else {
            res.json({
                ok: true,
                usuarios: usuarios
            });
        }
    });
});
evento.get('/eventos/:id', function (req, res) {
    var id = req.params.id;
    var escapeId = mysql_1.default.instance.connection.escape(id);
    var query = "SELECT *\n                 FROM eventos WHERE e_id = " + escapeId;
    mysql_1.default.ejecutarQuery(query, function (err, usuarios) {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        }
        else {
            res.json({
                ok: true,
                usuarios: usuarios
            });
        }
    });
});
exports.default = evento;

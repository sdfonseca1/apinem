"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var mysql_1 = __importDefault(require("../../mysql/mysql"));
var usuario = express_1.Router();
usuario.get('/usuarios', function (req, res) {
    var query = "SELECT *\n                 FROM usuarios";
    mysql_1.default.ejecutarQuery(query, function (err, usuarios) {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        }
        else {
            res.json({
                ok: true,
                usuarios: usuarios
            });
        }
    });
});
usuario.get('/usuarios/:id', function (req, res) {
    var id = req.params.id;
    var escapeId = mysql_1.default.instance.connection.escape(id);
    var query = "SELECT *\n                 FROM usuarios WHERE u_clave = " + escapeId;
    mysql_1.default.ejecutarQuery(query, function (err, usuarios) {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        }
        else {
            res.json({
                ok: true,
                usuarios: usuarios
            });
        }
    });
});
exports.default = usuario;

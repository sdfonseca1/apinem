"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = __importDefault(require("./server/server"));
var usuarios_1 = __importDefault(require("./router/usuarios"));
var mysql_1 = __importDefault(require("./mysql/mysql"));
var frecuencia_1 = __importDefault(require("./router/frecuencia"));
var server = server_1.default.init(3000);
server.app.use(usuarios_1.default);
server.app.use(frecuencia_1.default);
mysql_1.default.instance;
server.start(function () { return console.log('Servidor corriendo en el puerto 3000'); });

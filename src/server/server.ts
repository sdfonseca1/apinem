// require('./config/config');
import express = require('express');
import path = require('path');

export default  class Server {

  public app: express.Application;
  public port: number;

  constructor( port: number ){
    this.port = port;
    this.app = express();

  }

  static init ( port: number ){
    return new Server( port )
  }

  private publicFolder(){

    const publicPath = path.resolve(__dirname, '../public');
    this.app.use( express.static( publicPath ) );


  }

  start( callback: any){
    this.app.listen( this.port, callback );
    this.publicFolder();
  }

}








// const bodyParser = require('body-parser');

// const app = express();
// // Parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }));
// //Parse application/json
// app.use(bodyParser.json());


// app.get('/usuario', function (req, res) {
//   res.json('getUsuario');
// });

// app.post('/usuario', function (req, res) {
//   res.json('postUsuario');
// });

// app.put('/usuario/:id', function (req, res) {
//   let body = req.body;
//   if ( body.nombre === undefined) {
//     res.status(400).json({
//       ok: false,
//       message: "El nombre es necesario"
//     });
//   }else{
//     res.json({ persona : body });
//   }
//   let id = req.params.id;
  
// });

// app.delete('/usuario', function (req, res) {
//   res.json('delete Usuario');
// });

// app.listen(process.env.PORT, () =>{
//   console.log('Escuchando Puerto 3000');
// });
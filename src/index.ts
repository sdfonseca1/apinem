import Server from './server/server';
import usuario from './router/usuarios';
import MySQL from './mysql/mysql';
import frecuencia from './router/frecuencia';

const server = Server.init(3000);
server.app.use(usuario);
server.app.use(frecuencia);

MySQL.instance;

server.start( () => console.log('Servidor corriendo en el puerto 3000') );
import { Router, Request, Response } from 'express';
import MySQL from '../mysql/mysql';
const usuario = Router();

usuario.get('/usuarios', (req: Request, res: Response) => {

  const query = `SELECT *
                 FROM usuarios`;
  MySQL.ejecutarQuery( query, ( err: any, usuarios: Object[]) =>{
    console.log(usuarios);
    if ( err ){
      res.status(400).json({
        ok: false,
        error: err
      });
    }else {
      res.json({
        ok: true,
        usuarios
      })
    }
  });


});

usuario.get('/usuarios/:id', (req: Request, res: Response) => {

  const id = req.params.id;
  const escapeId = MySQL.instance.connection.escape( id );
  
  const query = `SELECT *
                 FROM usuarios WHERE u_clave = ${ escapeId }`;
  MySQL.ejecutarQuery( query, ( err: any, usuarios: Object[]) =>{
    if ( err ){
      res.status(400).json({
        ok: false,
        error: err
      });
    }else {
      res.json({
        ok: true,
        usuarios
      })
    }
  });

});

export default usuario;
import { Router, Request, Response } from 'express';
import MySQL from '../mysql/mysql';
const evento = Router();
const rutaPython = '../../dist/hola.py';

evento.get('/frecuencia/guardarConfig', (req: Request, res: Response) => {
  console.log("Entra a la API");
  let amplitud = req.query.amplitud;
  let frecuencia = req.query.frecuencia;
  console.log('Entra petición');
  const query = `INSERT INTO log VALUES(null, ${ frecuencia }, ${ amplitud }, null)`;
  MySQL.ejecutarQuery( query, ( err: any, response: any[]) =>{
    if ( err ){
      res.status(400).json({
        ok: false,
        error: err
      });
    }else {
      res.json({
        ok: true,
        response
      })
    }
  });
  console.log('Sale petición');


});

evento.get('/frecuencia/ejecutarScript', (req: Request, res: Response) => {
  
  const { spawn } = require('child_process'); 
    const pyprog = spawn('python',[rutaPython]); 

    pyprog.stdout.on('data', function( data: any ) { 

     return 1;

    }); 
    pyprog.stderr.on('data', ( data: any ) => { 

     return 0; 

    });

});

export default evento;